# sbt-confluent-hub-plugin

[![Download](https://api.bintray.com/packages/kpmeen/sbt-plugins/sbt-confluent-hub-plugin/images/download.svg)](https://bintray.com/kpmeen/sbt-plugins/sbt-confluent-hub-plugin/_latestVersion)

Confluent Inc. provides a central hub, called [Confluent Hub](https://www.confluent.io/hub/)
where different connectors and plugins for Kafka Connect are made available.
Both from commercial actors, and from the community.

This plugin implements the
[Confluent Hub Component Archive Specification](https://docs.confluent.io/current/connect/managing/confluent-hub/component-archive.html)
that is required to be able to have custom components published on the
Confluent Hub.

## Getting started

The `sbt-confluent-hub-plugin` currently has a dependency to the
[sbt-assembly](https://github.com/sbt/sbt-assembly) plugin. This might change in
future versions, but for now the Confluent Hub archive will contain a fat jar.
The plugin will pull in `sbt-assembly` if it is not already present in the build
configuration.

To add the plugin to your project, add the following snippet to your
`project/plugins.sbt` file.

This plugin requires sbt version `1.3.x` or newer.

```scala
addSbtPlugin("net.scalytica" % "sbt-confluent-hub-plugin" % "x.y.z")
```

Replace `x.y.z` with the latest version available on [bintray](https://bintray.com/kpmeen/sbt-plugins/sbt-confluent-hub-plugin/_latestVersion).

## Plugin Configuration

* `hubComponentTypes` *required*: Component types this archive contains. No default value.
* `hubOwnerUsername` *required*: Username of the component owner. No default value.
* `hubOwnerName` *required*: Name of the component owner. No default value.
* `hubArchiveDescription`: Description of the Confluent Hub component. Defaults to `description.value`.
* `hubDisplayTitle`: Display name for this component on Confluent Hub. Defaults to `name.value`.
* `hubComponentVersion`: The version of this component. Defaults to `version.value`.
* `hubComponentName`: Name of the archived component. Defaults to `name.value`
* `hubTags`: List of relevant search tags. Defaults to `Seq.empty`.
* `hubOwnerUrl`: A website or web page to associate with the owner. Defaults to `None`
* `hubOwnerLogo`: An image to associate with the component owner. Defaults to `None`
* `hubOwnerType`: The type of owner for the component. Defaults to `User`.
* `hubArchiveName`: The name of the Confluent Hub Archive. The default value is `s"${hubOwnerUsername.value}-${hubComponentName.value}-${hubComponentVersion.value}.zip"`.
* `hubRequirements`: A list of requirements given in a concise, bullet-listable format. Defaults to `Seq.empty`.
* `hubDocumentationUrl`: A link to documentation for the component. Defaults to `None`
* `hubLogo`: A logo to display on Confluent Hub for the component. Defaults to `None`
* `hubDocker`: Information on a Docker image available for the component. Defaults to `None`
* `hubFeatures`: These are specific features that are tracked by Confluent and used to help compare components. Any other features that make a component noteworthy should be detailed in the description field. Defaults to `None`.
* `hubComponentLicenses`: A list of licenses associated with the component. The example only contains one, but several can be specified if need be. It is strongly recommended that at least one license be provided. Defaults to value of `licenses.value`.
* `hubSupportProvider`: Information on who, if anyone, provides support for the component, and what kind of support is provided. Defaults to `None`.
* `hubArchiveDocFiles`: Files to include in the archive /doc directory. Defaults to `Seq.empty`.
* `hubArchiveEtcFiles`: Files to include in the archive /etc directory. Defaults to `Seq.empty`.
* `hubArchiveLibFiles`: Files to include in the archive /lib directory. Defaults to `Seq.empty`, but will include the jar file from `assembly` when running the `hubPackage` and `hubCreateManifest` tasks.
* `hubArchiveAssetsFiles`: Files to include in the archive /assets directory. Defaults to `Seq.empty`.
* `hubArchiveOutputDir`: Directory to create the Confluent Hub archive file. Defaults to `target.value`.

```
hubOwnerName: Knut Petter Meen
hubOwnerUsername: scalytica
hubArchiveDescription: kafka-connect-connector-params
hubComponentTypes: List(TransformComponent)
hubDisplayTitle: kafka-connect-connector-params
hubComponentVersion: 0.1.1-SNAPSHOT
hubComponentName: kafka-connect-connector-params
hubTags: List()
hubOwnerUrl: Some(https://scalytica.net)
hubOwnerLogo: None
hubArchiveName: scalytica-kafka-connect-connector-params-0.1.1-SNAPSHOT.zip
hubRequirements: List()
hubDocumentationUrl: None
hubLogo: None
hubDocker: None
hubFeatures: None
hubComponentLicenses: List(ComponentLicense(Apache-2.0,Some(http://opensource.org/licenses/https://opensource.org/licenses/Apache-2.0),None))
hubSupportProvider: None
Plugin attributes:
hubArchiveDocFiles: List(./LICENSE, ./README.md)
hubArchiveEtcFiles: List()
hubArchiveAssetsFiles: List()
```

## Plugin tasks

* `hubPackage`: Package the Confluent Hub archive.
* `hubCreateManifest`: Creates the Confluent Hub archive manifest.json to be included.
* `hubPrintSettings`: Print settings on CLI.
