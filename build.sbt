// scalastyle:off
import sbt.Keys._
import sbt._

import scala.xml.NodeSeq

name := """sbt-confluent-hub-plugin"""
organization := "net.scalytica"
description := "Sbt plugin for packaging Kafka Connect components, according" +
  " to the Confluent Hub archive specification, to be published on the " +
  "Confluent Hub."
licenses += ("Apache-2.0", url("https://opensource.org/licenses/Apache-2.0"))

scalaVersion := "2.12.10"

developers := List(
  Developer(
    id = "kpmeen",
    name = "Knut Petter Meen",
    email = "kp@scalytica.net",
    url = url("https://scalytica.net")
  )
)

sbtPlugin := true

scalacOptions := Seq(
  "-encoding",
  "utf-8",         // Specify character encoding used by source files.
  "-feature",      // Emit warning and location for usages of features that should be imported explicitly.
  "-deprecation",  // Emit warning and location for usages of deprecated APIs.
  "-unchecked",    // Enable additional warnings where generated code depends on assumptions.
  "-explaintypes", // Explain type errors in more detail.
  "-Xfuture",      // Turn on future language features.
  "-Xcheckinit",   // Wrap field accessors to throw an exception on uninitialized access.
//    "-Xfatal-warnings",                 // Fail the compilation if there are any warnings.
  "-Xlint:adapted-args",              // Warn if an argument list is modified to match the receiver.
  "-Xlint:by-name-right-associative", // By-name parameter of right associative operator.
  "-Xlint:constant",                  // Evaluation of a constant arithmetic expression results in an error.
  "-Xlint:delayedinit-select",        // Selecting member of DelayedInit.
  "-Xlint:doc-detached",              // A ScalaDoc comment appears to be detached from its element.
  "-Xlint:inaccessible",              // Warn about inaccessible types in method signatures.
  "-Xlint:infer-any",                 // Warn when a type argument is inferred to be `Any`.
  "-Xlint:missing-interpolator",      // A string literal appears to be missing an interpolator id.
  "-Xlint:nullary-override",          // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Xlint:nullary-unit",              // Warn when nullary methods return Unit.
  "-Xlint:option-implicit",           // Option.apply used implicit view.
  "-Xlint:package-object-classes",    // Class or object defined in package object.
  "-Xlint:poly-implicit-overload",    // Parameterized overloaded implicit methods are not visible as view bounds.
  "-Xlint:private-shadow",            // A private field (or class parameter) shadows a superclass field.
  "-Xlint:stars-align",               // Pattern sequence wildcard must align with sequence component.
  "-Xlint:type-parameter-shadow",     // A local type parameter shadows a type already in scope.
  "-Xlint:unsound-match",             // Pattern match may not be type-safe.
  "-language:implicitConversions",
  "-language:experimental.macros", // Allow macro definition (besides implementation and application)
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps",
  // Experimental options,
  "-Yno-adapted-args",       // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
  "-Ypartial-unification",   // Enable partial unification in type constructor inference
  "-Ywarn-dead-code",        // Warn when dead code is identified.
  "-Ywarn-value-discard",    // Warn when non-Unit expression results are unused.
  "-Ywarn-extra-implicit",   // Warn when more than one implicit parameter section is defined.
  "-Ywarn-numeric-widen",    // Warn when numerical values are widened.
  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports",   // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals",    // Warn if a local definition is unused.
  "-Ywarn-unused:params",    // Warn if a value parameter is unused.
  "-Ywarn-unused:patvars",   // Warn if a variable bound in a pattern is unused.
  "-Ywarn-unused:privates"   // Warn if a private member is unused.
)

javacOptions ++= Seq("-source", "11", "-target", "11")
javaOptions ++= Seq("-Duser.timezone=UTC")

updateOptions := updateOptions.value.withCachedResolution(true)

turbo := true

initialCommands in console := """import net.scalytica.sbt.confluenthub._"""

//// set up scripted sbt plugin for testing the sbt plugin
//scriptedLaunchOpts ++= Seq(
//  "-Xmx1024M",
//  "-Dplugin.version=" + version.value
//)

// Json4s dependencies
libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-core",
  "org.json4s" %% "json4s-native"
).map(_ % "3.6.7")

// ScalaTest
libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic",
  "org.scalatest" %% "scalatest"
).map(_ % "3.0.8" % Test)

// plugin dependencies
addSbtPlugin("com.eed3si9n" %% "sbt-assembly" % "0.14.10")

// Bintray settings
bintrayPackageLabels := Seq("sbt", "plugin", "kafka-connect", "confluent hub")
bintrayRepository := "sbt-plugins"
bintrayPackage := "sbt-confluent-hub-plugin"
bintrayOrganization in bintray := None
bintrayVcsUrl := Some("""git@gitlab.com:kpmeen/sbt-confluent-hub-plugin.git""")
bintrayReleaseOnPublish := false
pomExtra := {
  <url>https://gitlab.com/kpmeen/kafka-connect-connector-params</url>
  <scm>
    <url>git@gitlab.com:kpmeen/kafka-connect-connector-params.git</url>
    <connection>scm:git:git@gitlab.com:kpmeen/kafka-connect-connector-params.git</connection>
  </scm>
}
pomIncludeRepository := { _ =>
  false
}
autoAPIMappings := true
publishMavenStyle := false
publishArtifact in Test := false
publishArtifact in (Compile, packageSrc) := true
publishArtifact in (Compile, packageDoc) := false
publishArtifact in packageDoc := false
sources in (Compile, doc) := Seq.empty
