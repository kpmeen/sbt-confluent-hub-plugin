// scalastyle:off
logLevel := Level.Warn

resolvers ++= DefaultOptions.resolvers(snapshot = false)
resolvers ++= Seq(
  Resolver.typesafeRepo("releases"),
  Resolver.sonatypeRepo("releases"),
  // Remove below resolver once the following issues has been resolved:
  // https://issues.jboss.org/projects/JBINTER/issues/JBINTER-21
  "JBoss" at "https://repository.jboss.org/"
)

// Library dependencies
libraryDependencies += "org.scala-sbt" %% "scripted-plugin" % sbtVersion.value

// Dependency handling
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.0")

// Formatting and style checking
addSbtPlugin("org.scalameta"  % "sbt-scalafmt"           % "2.2.0")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")

// Code coverage
addSbtPlugin("org.scoverage" %% "sbt-scoverage" % "1.6.0")

// Release plugin
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.13")

// Bintray publishing
addSbtPlugin("org.foundweekends" % "sbt-bintray" % "0.5.6")
