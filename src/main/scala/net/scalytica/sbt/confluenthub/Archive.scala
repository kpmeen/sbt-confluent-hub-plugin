package net.scalytica.sbt.confluenthub

import java.nio.charset.Charset

import org.json4s.JsonAST
import org.json4s.native.JsonMethods._
import sbt._
import sbt.internal.util.ManagedLogger

private[confluenthub] object Archive {

  def create(
      targetDir: File,
      docFiles: Seq[File],
      etcFiles: Seq[File],
      libFiles: Seq[File],
      assetFiles: Seq[File],
      outputDir: File,
      archiveName: String
  )(implicit logger: ManagedLogger): File = {
    logger.info("Building assembly jar...")

    val buildDir =
      stageArchive(targetDir, docFiles, etcFiles, libFiles, assetFiles)

    logger.info("Staging of archive complete")
    val adf = outputDir / archiveName

    logger.info(s"Creating archive ${adf.getPath}...")
    FileOps.createConfluentHubArchive(
      stagingDir = buildDir,
      archiveFile = adf
    )
    logger.info(s"Archive ${adf.getPath} created")
    logger.info("------------------------------------------------")
    logger.warn(
      "To submit the Kafka Connect component to Confluent Hub, please " +
        "submit an email to confluent-hub@confluent.io with the " +
        s"${adf.getPath} attached."
    )
    logger.info("------------------------------------------------")

    adf
  }

  def createManifest(
      targetDir: File,
      content: JsonAST.JObject
  ): Unit = {
    val buildDir = FileOps.createBuildDirIfNotExists(targetDir)
    IO.write(
      file = buildDir / "manifest.json",
      content = pretty(render(content)),
      charset = Charset.forName("UTF-8"),
      append = false
    )
  }

  def stageArchive(
      targetDir: File,
      docFiles: Seq[File],
      etcFiles: Seq[File],
      libFiles: Seq[File],
      assetFiles: Seq[File]
  )(implicit logger: ManagedLogger): File = {
    val buildDir = FileOps.createBuildDirIfNotExists(targetDir)

    logger.info(s"Staging archive into ${buildDir.getPath}")

    FileOps.copyFiles(
      FileOps.createDirIfNotExists(buildDir, "doc")    -> docFiles,
      FileOps.createDirIfNotExists(buildDir, "etc")    -> etcFiles,
      FileOps.createDirIfNotExists(buildDir, "lib")    -> libFiles,
      FileOps.createDirIfNotExists(buildDir, "assets") -> assetFiles
    )
    buildDir
  }
}
