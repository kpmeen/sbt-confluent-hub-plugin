package net.scalytica.sbt.confluenthub

import sbt.{settingKey, taskKey, File}

trait ConfluentHubKeys {

  /* Task keys */
  lazy val hubPackage = taskKey[File](
    "Package the Confluent Hub archive"
  )

  lazy val hubCreateManifest = taskKey[Unit](
    "Creates the Confluent Hub archive manifest.json to be included"
  )

  lazy val hubPrintSettings = taskKey[Unit](
    "Print settings on CLI"
  )

  /* Files to include in the confluent hub archive  */
  lazy val hubArchiveDocFiles = settingKey[Seq[File]](
    "Files to include in the archive /doc directory"
  )

  lazy val hubArchiveEtcFiles = settingKey[Seq[File]](
    "Files to include in the archive /etc directory"
  )

  lazy val hubArchiveLibFiles = settingKey[Seq[File]](
    "Files to include in the archive /lib directory"
  )

  lazy val hubArchiveAssetsFiles = settingKey[Seq[File]](
    "Files to include in the archive /assets directory"
  )

  lazy val hubArchiveOutputDir = settingKey[File](
    "Directory to create the Confluent Hub archive file. Defaults to target."
  )

  /* Config keys */
  lazy val hubArchiveDescription = settingKey[String](
    "Description of the Confluent Hub component"
  )

  lazy val hubComponentTypes = settingKey[Seq[ConnectComponentType]](
    "Component types this archive contains"
  )

  lazy val hubDisplayTitle = settingKey[String](
    "Display name for this component on Confluent Hub"
  )

  lazy val hubComponentVersion = settingKey[String](
    "The version of this component"
  )

  lazy val hubComponentName = settingKey[String](
    "Name of the archived component"
  )

  lazy val hubTags = settingKey[Seq[String]](
    "List of relevant search tags"
  )

  lazy val hubOwnerUsername = settingKey[String](
    "Username of the component owner"
  )

  lazy val hubOwnerName = settingKey[String](
    "Name of the component owner"
  )

  lazy val hubOwnerUrl = settingKey[Option[String]](
    "A website or web page to associate with the owner"
  )

  lazy val hubOwnerLogo = settingKey[Option[String]](
    "An image to associate with the component owner."
  )

  lazy val hubOwnerType = settingKey[OwnerType](
    "The type of owner for the component. Defaults to user."
  )

  // ${componentOwner}-${componentName}-${componentVersion}.zip
  lazy val hubArchiveName = settingKey[String](
    "The name of the Confluent Hub Archive"
  )

  lazy val hubRequirements = settingKey[Seq[String]](
    "A list of requirements given in a concise, bullet-listable format"
  )

  lazy val hubDocumentationUrl = settingKey[Option[String]](
    "A link to documentation for the component"
  )

  lazy val hubLogo = settingKey[Option[String]](
    "A logo to display on Confluent Hub for the component"
  )

  lazy val hubDocker = settingKey[Option[Docker]](
    "Information on a Docker image available for the component"
  )

  lazy val hubFeatures = settingKey[Option[Features]](
    "These are specific features that are tracked by Confluent and used to" +
      " help compare components. Any other features that make a component" +
      " noteworthy should be detailed in the description field"
  )

  lazy val hubComponentLicenses = settingKey[Seq[ComponentLicense]](
    "A list of licenses associated with the component. The example only " +
      "contains one, but several can be specified if need be. It is " +
      "strongly recommended that at least one license be provided"
  )

  lazy val hubSupportProvider = settingKey[Option[Support]](
    "Information on who, if anyone, provides support for the component, " +
      "and what kind of support is provided"
  )
}

object ConfluentHubKeys extends ConfluentHubKeys
