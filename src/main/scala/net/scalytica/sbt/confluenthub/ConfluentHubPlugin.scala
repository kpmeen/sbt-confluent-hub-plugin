package net.scalytica.sbt.confluenthub

import org.json4s.JsonDSL._
import sbt._
import sbt.Keys._
import sbtassembly.AssemblyPlugin
import sbtassembly.AssemblyKeys._

object ConfluentHubPlugin extends AutoPlugin {

  import ConfluentHubKeys._

  override def requires = AssemblyPlugin

  // scalastyle:off method.length
  override def projectSettings = Seq(
    hubArchiveDescription := description.value,
    hubDisplayTitle := name.value,
    hubComponentVersion := version.value,
    hubComponentName := name.value,
    hubTags := Seq.empty,
    hubOwnerUrl := None,
    hubOwnerLogo := None,
    hubOwnerType := User,
    hubArchiveName := {
      s"${hubOwnerUsername.value}" +
        s"-${hubComponentName.value}" +
        s"-${hubComponentVersion.value}.zip"
    },
    hubRequirements := Seq.empty,
    hubDocumentationUrl := None,
    hubLogo := None,
    hubDocker := None,
    hubFeatures := None,
    hubComponentLicenses := licenses.value.map {
      case (l, u) => ComponentLicense(l, Option(u.toString), None)
    },
    hubSupportProvider := None,
    hubArchiveDocFiles := Seq.empty,
    hubArchiveEtcFiles := Seq.empty,
    hubArchiveLibFiles := Seq.empty,
    hubArchiveAssetsFiles := Seq.empty,
    hubArchiveOutputDir := target.value,
    hubPrintSettings := {
      val logger = streams.value.log
      logger.info {
        "------------------------------------------------\n" +
          "Confluent Hub Archive properties:\n" +
          s"hubOwnerName: ${hubOwnerName.value}\n" +
          s"hubOwnerUsername: ${hubOwnerUsername.value}\n" +
          s"hubArchiveDescription: ${hubArchiveDescription.value}\n" +
          s"hubComponentTypes: ${hubComponentTypes.value}\n" +
          s"hubDisplayTitle: ${hubDisplayTitle.value}\n" +
          s"hubComponentVersion: ${hubComponentVersion.value}\n" +
          s"hubComponentName: ${hubComponentName.value}\n" +
          s"hubTags: ${hubTags.value}\n" +
          s"hubOwnerUrl: ${hubOwnerUrl.value}\n" +
          s"hubOwnerLogo: ${hubOwnerLogo.value}\n" +
          s"hubArchiveName: ${hubArchiveName.value}\n" +
          s"hubRequirements: ${hubRequirements.value}\n" +
          s"hubDocumentationUrl: ${hubDocumentationUrl.value}\n" +
          s"hubLogo: ${hubLogo.value}\n" +
          s"hubDocker: ${hubDocker.value}\n" +
          s"hubFeatures: ${hubFeatures.value}\n" +
          s"hubComponentLicenses: ${hubComponentLicenses.value}\n" +
          s"hubSupportProvider: ${hubSupportProvider.value}\n" +
          "Plugin specific attributes:\n" +
          s"hubArchiveDocFiles: ${hubArchiveDocFiles.value}\n" +
          s"hubArchiveEtcFiles: ${hubArchiveEtcFiles.value}\n" +
          s"hubArchiveLibFiles: ${hubArchiveLibFiles.value}\n" +
          s"hubArchiveAssetsFiles: ${hubArchiveAssetsFiles.value}\n" +
          "------------------------------------------------"
      }
    },
    hubCreateManifest := {
      val json =
        ("name"                -> hubComponentName.value) ~
          ("title"             -> hubDisplayTitle.value) ~
          ("version"           -> hubComponentVersion.value) ~
          ("description"       -> hubArchiveDescription.value) ~
          ("component_types"   -> hubComponentTypes.value.map(_.value)) ~
          ("documentation_url" -> hubDocumentationUrl.value) ~
          ("logo"              -> hubLogo.value) ~
          ("license"           -> hubComponentLicenses.value.map(_.asJson)) ~
          ("docker_image"      -> hubDocker.value.map(_.asJson)) ~
          ("features"          -> hubFeatures.value.map(_.asJson)) ~
          ("owner" ->
            ("name"       -> hubOwnerName.value) ~
              ("username" -> hubOwnerUsername.value) ~
              ("type"     -> hubOwnerType.value.value) ~
              ("url"      -> hubOwnerUrl.value) ~
              ("logo"     -> hubOwnerLogo.value)) ~
          ("requirements" -> hubRequirements.value) ~
          ("support"      -> hubSupportProvider.value.map(_.asJson)) ~
          ("tags"         -> hubTags.value)

      Archive.createManifest(target.value, json)
    },
    hubPackage := {
      implicit val logger = streams.value.log

      hubCreateManifest.value

      Archive.create(
        targetDir = target.value,
        docFiles = hubArchiveDocFiles.value,
        etcFiles = hubArchiveEtcFiles.value,
        libFiles = hubArchiveLibFiles.value :+ assembly.value,
        assetFiles = hubArchiveAssetsFiles.value,
        outputDir = hubArchiveOutputDir.value,
        archiveName = hubArchiveName.value
      )
    }
  )
  // scalastyle:on method.length
}
