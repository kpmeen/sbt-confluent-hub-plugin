package net.scalytica.sbt.confluenthub

import sbt._

private[confluenthub] object FileOps {

  def createBuildDirIfNotExists(parent: File): File =
    createDirIfNotExists(parent, "hub-archive")

  def copyFiles(destAndFiles: (File, Seq[File])*): Unit =
    destAndFiles.foreach {
      case (toDir, files) =>
        files.foreach { f =>
          IO.copyFile(
            sourceFile = f,
            targetFile = toDir / f.getName,
            preserveLastModified = true,
            preserveExecutable = true
          )
        }
    }

  def createDirIfNotExists(buildDir: File, dir: String): File = {
    val d = buildDir / dir
    if (!d.exists()) d.mkdir()
    d
  }

  def createConfluentHubArchive(
      stagingDir: File,
      archiveFile: File
  ): Unit = IO.zip(Path.allSubpaths(stagingDir), archiveFile)

}
