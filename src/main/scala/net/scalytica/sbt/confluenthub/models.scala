package net.scalytica.sbt.confluenthub

import org.json4s._
import org.json4s.JsonDSL._

sealed abstract class ConnectComponentType(val value: String)
case object SinkComponent      extends ConnectComponentType("sink")
case object SourceComponent    extends ConnectComponentType("source")
case object TransformComponent extends ConnectComponentType("transform")
case object ConverterComponent extends ConnectComponentType("converter")

sealed abstract class OwnerType(val value: String)
case object Organisation extends OwnerType("organization")
case object User         extends OwnerType("user")

sealed abstract class DeliveryGuarantee(val value: String)
case object AtLeastOnce extends DeliveryGuarantee("at_least_once")
case object ExactlyOnce extends DeliveryGuarantee("exactly_once")

case class ComponentLicense(
    name: String,
    url: Option[String],
    logo: Option[String]
) {

  private[confluenthub] def asJson: JsonAST.JObject = {
    ("name"   -> name) ~
      ("url"  -> url) ~
      ("logo" -> logo)
  }
}

case class Support(
    providerName: String,
    summary: Option[String],
    url: Option[String],
    logo: Option[String]
) {

  private[confluenthub] def asJson: JsonAST.JObject = {
    ("provider_name" -> providerName) ~
      ("summary"     -> summary) ~
      ("url"         -> url) ~
      ("logo"        -> logo)
  }
}

case class Features(
    encodings: Seq[String],
    guarantees: Seq[DeliveryGuarantee] = Seq.empty,
    kafkaConnectApi: Boolean = true,
    supportsControlCenter: Boolean = false,
    singleMessageTransforms: Boolean = true
) {

  private[confluenthub] def asJson: JsonAST.JObject = {
    ("confluent_control_center_integration" -> supportsControlCenter) ~
      ("delivery_guarantee"                 -> guarantees.map(_.value)) ~
      ("kafka_connect_api"                  -> kafkaConnectApi) ~
      ("single_message_transforms"          -> singleMessageTransforms) ~
      ("supported_encodings"                -> encodings)
  }
}

case class Docker(
    namespace: String,
    name: String,
    tag: Option[String],
    registry: Option[String]
) {

  private[confluenthub] def asJson: JsonAST.JObject = {
    ("namespace"  -> namespace) ~
      ("name"     -> name) ~
      ("tag"      -> tag) ~
      ("registry" -> registry)
  }
}
